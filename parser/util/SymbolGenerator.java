package parser.util;

import symbol.SymbolTable;

import java.util.ArrayList;

public class SymbolGenerator {

    public void generateSymbols(ArrayList<String> linesFromFile) {
        int instructionCount = 0;
        int addressCount = 16;

        for (String line : linesFromFile) {
            if (!line.startsWith("(")) instructionCount++;

            else if (line.startsWith("(")) {
                SymbolTable.addLabelToSymbol(line.substring(1, line.length() - 1), instructionCount);
            }
        }

        for(String line: linesFromFile){
            if(line.startsWith("@")){
                if(!isAddress(line.substring(1))) {
                    if (!SymbolTable.symbolExists(line.substring(1))) {
                        SymbolTable.addLabelToSymbol(line.substring(1), addressCount);
                        addressCount++;
                    }
                }
            }
        }
    }

    private boolean isAddress(String aInstruction){
        try{Integer.parseInt(aInstruction);}
        catch(NumberFormatException ex){
            return false;
        }
        return  true;
    }
}
