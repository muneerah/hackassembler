package parser.util;

import java.util.ArrayList;

public class ProgramCleaner {

    public void removeInvalidLines(ArrayList<String> linesFromFile){
        linesFromFile.removeIf(line->line.isEmpty() || line.startsWith("//"));
    }

    public void removeInlineComments(ArrayList<String> linesFromFile){
        linesFromFile.replaceAll(line->line.contains("//") ? line.substring(0,line.indexOf("//")).trim():line.trim());
    }
}
