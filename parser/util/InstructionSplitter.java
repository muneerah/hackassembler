package parser.util;

import parser.instruction.AInstruction;
import parser.instruction.CInstruction;
import parser.instruction.Instruction;

import java.util.ArrayList;

public class InstructionSplitter {

    public ArrayList<Instruction> createInstructionList(ArrayList<String> linesFromFile){
        ArrayList<Instruction> instructionList = new ArrayList<>();
        linesFromFile.forEach(line->{
            if(!line.startsWith("(")) {
                Instruction instruction;
                if (line.startsWith("@")) {
                    instruction = new AInstruction(line.substring(1));
                    instruction.setLineFromFile(line);
                } else {
                    instruction = createCInstruction(line);
                    instruction.setLineFromFile(line);
                }
                instructionList.add(instruction);
            }
        });
        return instructionList;
    }

    private CInstruction createCInstruction(String line){
        String dest = line.contains("=") ? line.substring(0, line.indexOf("=")) : null;
        String jmp = line.contains(";") ? line.substring(line.indexOf(";")+1) : null;
        String cmp = dest==null ? line.substring(0, line.indexOf(";")) : line.substring(line.indexOf("=")+1);
        return new CInstruction(dest, cmp, jmp);
    }
}

