package parser.instruction;

import parser.enums.InstructionType;

public class AInstruction extends Instruction {

    private String value;

    public AInstruction(String value){
        this.value=value;
        this.type= InstructionType.A;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AInstruction{" +
                "lineFromFile='" + lineFromFile + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
