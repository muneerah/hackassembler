package parser.instruction;


import parser.enums.InstructionType;

public abstract class Instruction {

    String lineFromFile;
    InstructionType type;
    String binaryRepresentation;

    public String getLineFromFile() {
        return lineFromFile;
    }

    public void setLineFromFile(String lineFromFile) {
        this.lineFromFile = lineFromFile;
    }


    public InstructionType getType() {
        return type;
    }

    public void setType(InstructionType type) {
        this.type = type;
    }

    public boolean isAInstruction(){
        return InstructionType.A.equals(this.type);
    }

    public String getBinaryRepresentation() {
        return binaryRepresentation;
    }

    public void setBinaryRepresentation(String binaryRepresentation) {
        this.binaryRepresentation = binaryRepresentation;
    }
}
