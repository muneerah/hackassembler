package parser.instruction;

import parser.enums.InstructionType;

public class CInstruction extends Instruction {

    private String comp;
    private String dest;
    private String jump;

    public CInstruction(String dest, String comp, String jump){
        this.comp = comp;
        this.dest = dest;
        this.jump = jump;
        this.type = InstructionType.C;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getJump() {
        return jump;
    }

    public void setJump(String jump) {
        this.jump = jump;
    }

    @Override
    public String toString() {
        return "CInstruction{" +
                "lineFromFile='" + lineFromFile + '\'' +
                ", comp='" + comp + '\'' +
                ", dest='" + dest + '\'' +
                ", jump='" + jump + '\'' +
                '}';
    }
}
