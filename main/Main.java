package main;

import code.InstructionConverter;
import parser.instruction.Instruction;
import parser.util.InstructionSplitter;
import parser.util.ProgramCleaner;
import parser.util.SymbolGenerator;

import java.io.*;
import java.util.ArrayList;

public class Main {

    public static void main(String args[]) throws IOException {

        ArrayList<String> linesOfFile = loadFileContents();

        ProgramCleaner programCleaner = new ProgramCleaner();
        programCleaner.removeInvalidLines(linesOfFile);
        programCleaner.removeInlineComments(linesOfFile);

        SymbolGenerator symbolGenerator = new SymbolGenerator();
        symbolGenerator.generateSymbols(linesOfFile);

        linesOfFile.forEach(System.out::println);

        InstructionSplitter instructionSplitter = new InstructionSplitter();
        ArrayList<Instruction> instructions =  instructionSplitter.createInstructionList(linesOfFile);

        InstructionConverter converter = new InstructionConverter();

        BufferedWriter writer = new BufferedWriter(new FileWriter("output.hack"));

        instructions.forEach(instruction -> {
            converter.convertInstruction(instruction);
            System.out.println(instruction.getBinaryRepresentation());
            try {
                writer.write(instruction.getBinaryRepresentation()+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        writer.close();
    }

    public static ArrayList loadFileContents() throws IOException {
        File file = new File("C:\\dev\\nand2tetris\\projects\\06\\rect\\Rect.asm");
        BufferedReader br = new BufferedReader(new FileReader(file));
        ArrayList<String> linesFromFile = new ArrayList<>();
        String lineFromFile;

        while((lineFromFile = br.readLine()) != null){
            linesFromFile.add(lineFromFile);
        }

        br.close();
        return linesFromFile;
    }
}
