package code;


import parser.enums.InstructionType;
import parser.instruction.AInstruction;
import parser.instruction.CInstruction;
import parser.instruction.Instruction;
import symbol.SymbolTable;

import java.util.HashMap;

public class InstructionConverter {

    private static final HashMap<String, String> destConverter;

    static {
        destConverter = new HashMap<>();
        destConverter.put(null, "000");
        destConverter.put("M", "001");
        destConverter.put("D", "010");
        destConverter.put("MD", "011");
        destConverter.put("A", "100");
        destConverter.put("AM", "101");
        destConverter.put("AD", "110");
        destConverter.put("AMD", "111");
    }

    private static final HashMap<String, String> jmpConverter;

    static {
        jmpConverter = new HashMap<>();
        jmpConverter.put(null, "000");
        jmpConverter.put("JGT", "001");
        jmpConverter.put("JEQ", "010");
        jmpConverter.put("JGE", "011");
        jmpConverter.put("JLT", "100");
        jmpConverter.put("JNE", "101");
        jmpConverter.put("JLE", "110");
        jmpConverter.put("JMP", "111");
    }

    private static final HashMap<String, String> compConverter;

    static {
        compConverter = new HashMap<>();
        compConverter.put("0", "0101010");
        compConverter.put("1", "0111111");
        compConverter.put("-1", "0111010");
        compConverter.put("D", "0001100");
        compConverter.put("A", "0110000");
        compConverter.put("!D", "0001101");
        compConverter.put("!A", "0110001");
        compConverter.put("-D", "0001111");
        compConverter.put("-A", "0110011");
        compConverter.put("D+1", "0011111");
        compConverter.put("A+1", "0110111");
        compConverter.put("D-1", "0001110");
        compConverter.put("A-1", "0110010");
        compConverter.put("D+A", "0000010");
        compConverter.put("D-A", "0010011");
        compConverter.put("A-D", "0000111");
        compConverter.put("D&A", "0000000");
        compConverter.put("D|A", "0010101");

        compConverter.put("M", "1110000");
        compConverter.put("!M", "1110001");
        compConverter.put("-M", "1110011");
        compConverter.put("M+1", "1110111");
        compConverter.put("M-1", "1110010");
        compConverter.put("D+M", "1000010");
        compConverter.put("D-M", "1010011");
        compConverter.put("M-D", "1000111");
        compConverter.put("D&M", "1000000");
        compConverter.put("D|M", "1010101");
    }

    public void convertInstruction(Instruction instruction) {
        if (InstructionType.A.equals(instruction.getType())) convertAInstruction((AInstruction) instruction);
        else convertCInstruction((CInstruction) instruction);
    }

    private void convertCInstruction(CInstruction instruction) {
        String binaryRepresentation = "111";
        binaryRepresentation += compConverter.get(instruction.getComp());
        binaryRepresentation += destConverter.get(instruction.getDest());
        binaryRepresentation += jmpConverter.get(instruction.getJump());

        instruction.setBinaryRepresentation(binaryRepresentation);
    }

    private void convertAInstruction(AInstruction instruction) {

        Integer address;
        if (isAddress(instruction.getValue())) {
            address = Integer.valueOf(instruction.getValue());
        } else {
            address = SymbolTable.getAddress(instruction.getValue());
        }

        String binaryRepresentation = new String();
        int binaryLength = Integer.toBinaryString(address).length();
        int padding = 16 - binaryLength;

        for (int i = 0; i < padding; i++) {
            binaryRepresentation += "0";
        }

        binaryRepresentation += Integer.toBinaryString(address);
        instruction.setBinaryRepresentation(binaryRepresentation);
    }


    private boolean isAddress(String aInstruction){
        try{Integer.parseInt(aInstruction);}
        catch(NumberFormatException ex){
            return false;
        }
        return  true;
    }
}
